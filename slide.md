---
theme : "night"
customTheme : "slide"
transition: "slide"
highlightTheme: "darkula"
---

<!-- .slide: data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
## INTRODUCTION TO 
# <span class="element blue">**DOCKER**</span>
### The story of a whale
<small>From Anh Tu Nguyen. With 🧡</small>

---
<!-- .slide: data-transition="slide" data-background="#26145c" data-background-transition="zoom" data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
## REAL LIFE ASSIGNMENTS


----

### ASSIGNMENT 0


----

## Install 
# SQLServer <!-- .element: class="fragment fade-up" -->


----

### You cannot uninstall SQLServer completely without consequences.
https://www.red-gate.com/simple-talk/blogs/uninstalling-sql-server-pain/

----

## Don't try it at your PC

----

### ASSIGNMENT 1

----

### Mr.LEAD wants you to: 
<!-- .element: class="fragment fade-up" -->

----

- Make an ASP.NET Core application
- Connect to PostgreSQL (Not SQLServer - it's pricey)
- Separate the static server
- Use Redis as Cache

----

## First, you thought it's easy
# VM ftw
### vmware, virtualbox, parallel, qemu

----

<img width="100%" src="https://media1.giphy.com/media/ddHhhUBn25cuQ/giphy.gif">

----

## A few days later
### Mr.LEAD continues: <!-- .element: class="fragment fade-up" -->

----

- Update ASP.NET Core version and dependencies 
- Update static server
- Update Redis to latest version because the last one was buggy

----

- Test with several PostgreSQL versions (to see if any performance impacts due to recent upgrades)

----

<img width="100%" src="https://media3.giphy.com/media/27EhcDHnlkw1O/giphy.gif">

----

### ASSIGNMENT 2
----

- We have a tested environment somehow.
- Make the production environment exactly like the development environment version
----

### ASSIGNMENT 3
----

- Let's do a load balancer which will help us scale horizontally
- Add more instances of our services
----

### ASSIGNMENT 4
----

- We have a database setup for testing
- We want to do several test case, every test case will begin with a certain state of the test database
* Scripting works, but it'll takes more effort to test
----


# Using VMs
## We CAN NOT
## or A LOT of efforts.
---
<!-- .slide: data-transition="slide" data-background="#26145c" data-background-transition="zoom" data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
# Enter Docker
---
<!-- .slide: data-transition="slide" data-background="#26145c" data-background-transition="zoom" data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
# Comparision
## VM vs Docker
----

### Docker  = Process-Based virtualization

### VM      = OS-Based virtualization
----

###  Union filesystem
----

<img src="./_assets/images/container.png">
---
<!-- .slide: data-transition="slide" data-background="#26145c" data-background-transition="zoom" data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
# Benefits
----

## Resources usage
- Share the same layer (base image)
----

### We BUILD the VM
- Changes can be made to the base image
- Different with Puppets, Chef (We run the script to update VMs)
----

### Reusable
- Be able to pack up and share/deploy anywhere
----

### Completely same runtime for our application
- Docker wins in every aspect of production mode
----

### Deployment team can drink coffee
## and 
# growing hair
----

### Making micro-services easier to maintain
- By explicitly define the domain for every service
- It's micro size, make the team more focus
----

### Quantiative Resource Usage
- PaaS && IaaS: how much does one request cost?
- FaaS: how much does one function cost?
----

### API-based Development
- Make interfaces, schematic, etc kind of contract-based finest.
---
<!-- .slide: data-transition="slide" data-background="#26145c" data-background-transition="zoom" data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
# VMs still makes sense
----

# IF
----

### We're in ASML team
- means, we need an isolated environment for developing
- Monolithic applications
- Security
---
<!-- .slide: data-transition="slide" data-background="#26145c" data-background-transition="zoom" data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
## PART2 - PRACTICE/DEMO
> #### We shall not have a beautiful pottery without getting hands dirty

----

### Docker Toolbox vs Docker For Windows
----

#### Docker Toolbox
- You run an vm with linux core as guest
- OS as host will interact with the vm through docker-machine
- Resources depend on VM resources
----

#### Docker For Windows
- Use Hyper-V
- Native support to window kernel
- Better performance in term of resource usage
- Integrated best with Windows Server
----

### ... But I don't like Hyper-V

----

#### Inside a VM Linux machine
- You can turn on VT-X/AMD-V for a VM linux to let it run another virtualization layer
- Install docker
- Docker support natively on Linux
- You got a complete native experience, without Hyper-V

----

### Dockerfile, <br> docker-compose.yml, <br> WTF?
### How do I start?
----

### Dockerfile
To build one base image (for resuse later)
----

```Dockerfile
FROM ubuntu:15.04
COPY . /app
RUN make /app
CMD python /app/app.py
```
----

<img src="./_assets/images/container-layers.jpg">
----

### Docker-compose
To build a couple images into one working stack with dependencies

```YAML
postgres:
    image: postgres
    environment:
        POSTGRES_DB: taigadb
        POSTGRES_PASSWORD: password
        POSTGRES_USER: postgres
    ports:
        - 5432
    volumes:
        - ./pgdata:/var/lib/postgresql/data
```
---
<!-- .slide: data-transition="slide" data-background="#26145c" data-background-transition="zoom" data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
# REFS
1. [Introduction to containers and concept: Pros and Cons](https://medium.com/flow-ci/introduction-to-containers-concept-pros-and-cons-orchestration-docker-and-other-alternatives-9a2f1b61132c)

2. [How to build application with Docker Compose](https://www.sumologic.com/blog/devops/how-to-build-applications-docker-compose/)
----
## This slides && Sample projects
https://gitlab.com/Sioux.NX/NX.Workshop/IntroductionToDocker

---
<!-- .slide: data-transition="slide" data-background="#26145c" data-background-transition="zoom" data-background-video="./_assets/background_magenta.webm" data-background-video-loop="loop" -->
# DANKS