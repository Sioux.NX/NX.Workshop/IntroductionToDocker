1. Create `Dockerfile`
2. Create `requirements.txt`
3. Create `docker-compose.yml`
4. RUN `docker-compose run web django-admin.py startproject composeexample .`
5. Edit `settings.py` for Database connection:
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}
```
6. Add allowed host in `settings.py`
```
 ALLOWED_HOSTS = ['*']
 ```
 7. Try `docker-compose up`
 8. Voila!