- install reveal-md 
```Bash
npm i -g reveal-md
```

- build:
```
reveal-cmd slide.md --css _assets/slide.css --static dist
```

- copy _assets to dist